CC 			= gcc
CFLAGS		= -Wall -s -O3
RM          = rm -v

GIT_HASH	= `git describe`
GIT_BRANCH	= `git rev-parse --abbrev-ref HEAD`
export VERSION_FLAGS=-DGIT_HASH="\"$(GIT_HASH)\"" -DGIT_BRANCH="\"$(GIT_BRANCH)\""

all:
	$(CC) src/main.c pt_memalloc.c $(VERSION_FLAGS) -o femto $(CFLAGS)

clean:
	-@$(rm) femto
