
#include <stdint.h>
#include <string.h>
#ifndef PTMEM_SIZE
#define PTMEM_SIZE 150
#endif

#define HEAP_SIZE 1024 * PTMEM_SIZE 


#ifndef NULL
#define NULL ((void *)0)
#endif
#undef sbrk
// typedef long intptr_t;
char HEAP[HEAP_SIZE] = { 0 }; 

typedef char ALIGN[32];


union header {
	struct {
		unsigned int size;
		unsigned int is_free;
		union header *next;
	} s;
	/* force the header to be aligned to 16 bytes */
	ALIGN stub;
};
typedef union header header_t;
#define HEADER_SIZE sizeof(header_t)

header_t *head = NULL, *tail = NULL;

int align(int n) {
  return (n + sizeof(double) - 1) & ~(sizeof(double) - 1);
}

static char* pt_mem_index = HEAP;
static uint32_t pt_mem_used = 0;

static void *sbrk(short increment)
{
	char* start = pt_mem_index;
	if (increment == 0) return (void*)pt_mem_index;
	if (increment > 0)
	{
		if ((char*)(pt_mem_index + increment) > &HEAP[HEAP_SIZE]) return (void*) -1;
	} else {
		if ((char*)(pt_mem_index + increment) < &HEAP[0]) return (void*) -1;
	}
	pt_mem_index += increment;
	pt_mem_used += increment;
	return (void *)start;
}

header_t *get_free_block(size_t size)
{
	header_t *curr = head;
	while(curr) {
		/* see if there's a free block that can accommodate requested size */
		if (curr->s.is_free && curr->s.size >= size)
			return curr;
		curr = curr->s.next;
	}
	return NULL;
}

void pt_free(void *block)
{
	header_t *header, *tmp;
	/* program break is the end of the process's data segment */
	void *programbreak;

	if (!block)
		return;
	// pthread_mutex_lock(&global_malloc_lock);
	header = (header_t*)block - 1;
	/* sbrk(0) gives the current program break address */
	programbreak = sbrk(0);

	/*
	   Check if the block to be freed is the last one in the
	   linked list. If it is, then we could shrink the size of the
	   heap and release memory to OS. Else, we will keep the block
	   but mark it as free.
	 */
	if ((char*)block + header->s.size == programbreak) {
		if (head == tail) {
			head = tail = NULL;
		} else {
			tmp = head;
			while (tmp) {
				if(tmp->s.next == tail) {
					tmp->s.next = NULL;
					tail = tmp;
				}
				tmp = tmp->s.next;
			}
		}
		/*
		   sbrk() with a negative argument decrements the program break.
		   So memory is released by the program to OS.
		*/
		sbrk(0 - header->s.size - sizeof(header_t));
		/* Note: This lock does not really assure thread
		   safety, because sbrk() itself is not really
		   thread safe. Suppose there occurs a foregin sbrk(N)
		   after we find the program break and before we decrement
		   it, then we end up realeasing the memory obtained by
		   the foreign sbrk().
		*/
		//pthread_mutex_unlock(&global_malloc_lock);
		// pt_mem_used -= header->s.size + sizeof(header_t);
		return;
	}
	header->s.is_free = 1;
	//pt_mem_used -= header->s.size;
	//pthread_mutex_unlock(&global_malloc_lock);
}

void *pt_malloc(size_t size)
{
	size_t total_size;
	void *block;
	header_t *header;
	if (!size)
		return NULL;
	size = align(size);
	//pthread_mutex_lock(&global_malloc_lock);
	header = get_free_block(size);
	if (header) {
		/* Woah, found a free block to accommodate requested memory. */
		// pt_mem_used += header->s.size + sizeof(header_t);
		header->s.is_free = 0;
		//pthread_mutex_unlock(&global_malloc_lock);
		return (void*)(header + 1);
	}
	/* We need to get memory to fit in the requested block and header from OS. */
	total_size = sizeof(header_t) + size ;
	block = sbrk(total_size);
	if (block == (void*) -1) {
		//pthread_mutex_unlock(&global_malloc_lock);
		return NULL;
	}
	// pt_mem_used += total_size;
	header = block;
	header->s.size = size;
	header->s.is_free = 0;
	header->s.next = ((void*)0);
	if (!head)
		head = header;
	if (tail)
		tail->s.next = header;
	tail = header;
	// pthread_mutex_unlock(&global_malloc_lock);
	return (void*)(header + 1);
}

void *pt_calloc(size_t num, size_t nsize)
{
	size_t size;
	void *block;
	if (!num || !nsize)
		return NULL;
	size = num * nsize;
	/* check mul overflow */
	if (nsize != size / num)
		return NULL;
	block = pt_malloc(size);
	if (!block)
		return NULL;
	memset(block, 0, size);
	return block;
}

void *pt_realloc(void *block, size_t size)
{
	header_t *header;
	void *ret;
	if (!block || !size)
		return pt_malloc(size);
	header = (header_t*)block - 1;
	if (header->s.size >= size)
		return block;
	ret = pt_malloc(size);
	if (ret) {
		/* Relocate contents to the new bigger block */
		memcpy(ret, block, header->s.size);
		/* Free the old memory block */
		pt_free(block);
	}
	return ret;
}

void pt_reset_memory()
{
	memset(HEAP,0,HEAP_SIZE);
	pt_mem_index = HEAP;
	pt_mem_used = 0;
}

uint32_t pt_get_used_memory()
{
	return pt_mem_used;
}
uint32_t pt_get_free_memory()
{
	return HEAP_SIZE - pt_mem_used;
}
