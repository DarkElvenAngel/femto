#ifndef _PT_MEM_H
#define _PT_MEM_H
#include <stdint.h>
#include <string.h>

void *pt_realloc(void *block, size_t size);
void *pt_calloc(size_t num, size_t nsize);
void *pt_malloc(size_t size);
void pt_free(void *block);
void pt_reset_memory();
uint32_t pt_get_used_memory();
uint32_t pt_get_free_memory();

#undef malloc
#undef calloc
#undef realloc
#undef free
#define malloc pt_malloc
#define calloc pt_calloc
#define realloc pt_realloc
#define free pt_free



#endif