#ifndef __LINE_EDITOR_H
#define __LINE_EDITOR_H

#include <stdint.h>
#include <stdbool.h>

void Line_Editor_Process_Keypress(uint8_t margin, bool skip_clean);
char* Line_Editor_ReadLine(const char* prompt);

#endif