#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "LineEditor.h"


#if USE_PTMEM == 1
  #include "../pt_mem.h"
#endif

#if FEMTO_PICO_LIB == 1
#ifdef USE_PT_IO
  #include "pt52io.h"
  #define USE_PTMEM 1
  #define FEMTO_malloc pt_malloc
  #define FEMTO_calloc pt_calloc
  #define FEMTO_realloc pt_realloc
  #define FEMTO_free pt_free
#else
  #define USE_PTMEM 0
#endif
#else
  #include <sys/ioctl.h>
  #include <sys/types.h>
  #include <termios.h>
  #include <locale.h>
  #include <fcntl.h>
  struct termios orig_termios;
  #undef puts
  #define puts(a) printf("%s\n\r",a)
#endif

#ifndef GIT_BRANCH
#define GIT_BRANCH "" 
#endif
#ifndef GIT_HASH
#define GIT_HASH ""
#endif

#ifndef FEMTO_MAX_COL
#define FEMTO_MAX_COL 80
#endif
#ifndef FEMTO_CACHE_SIZE
#define FEMTO_CACHE_SIZE 1024
#endif

#ifndef FEMTO_malloc
#define FEMTO_malloc malloc
#endif
#ifndef FEMTO_calloc
#define FEMTO_calloc calloc
#endif
#ifndef FEMTO_realloc
#define FEMTO_realloc realloc
#endif
#ifndef FEMTO_free
#define FEMTO_free free
#endif
