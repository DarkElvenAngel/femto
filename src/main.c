/**
 * @file main.c
 * @author Dark Elven Angel
 * @brief Femto text editor
 * @version 0.2
 * @date 2023-06-21
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include "common.h"


#define FEMTO_VERSION "FEMTO " GIT_HASH
#define FEMTO_TABSTOP 4
#define FEMTO_MAX_FNL 256

#define POS_HEAD 0
#define POS_TAIL -1


#pragma pack(1)
typedef struct Femto_line
{
    struct Femto_line   *next;
    struct Femto_line   *prev;
    uint32_t      pos;
    uint16_t      len;
    uint8_t       flags;
} erow_t;

struct editorConfig
{
    bool running;
    bool dirty;
    FILE* origin;   
    FILE* working;   
    char* origin_fn;
    char* working_fn;
    erow_t *head;
    erow_t *tail;
    char* cache;
#ifdef FEMTO_WRITE_BUFFER_SIZE
    char* write_buffer;
#endif
    uint16_t cache_len;
    uint16_t current_line;
    uint16_t count;
    uint16_t screen_cols;           // How many Columns on screen [WIDTH]
} E;

extern char buf[];
extern char rbuf[];


void die(const char *s, const char *s1) {
  printf("ERROR :  %s  in %s\n\r", s,s1);
  exit(1);
}

void error_message(const char* msg)
{
    printf("\x1b[31;1mERROR: \x1b[m%s\n\r", msg);
}

void prompt_input(const char* msg)
{
    printf("\x1b[33;1m%s\x1b[m ", msg);
    Line_Editor_Process_Keypress(strlen(msg) + 1, false);
}

#if FEMTO_PICO_LIB == 0
void disableRawMode() {
  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios) == -1)
    die("tcsetattr", __FUNCTION__);
}

void enableRawMode() {
  if (tcgetattr(STDIN_FILENO, &orig_termios) == -1) die("tcgetattr", __FUNCTION__);
  atexit(disableRawMode);

  struct termios raw = orig_termios;
  raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
  raw.c_oflag &= ~(OPOST);
  raw.c_cflag |= (CS8);
  raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
  raw.c_cc[VMIN] = 0;
  raw.c_cc[VTIME] = 1;

  if (tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw) == -1) die("tcsetattr", __FUNCTION__);
}
#endif
void Femto_Delete_Line(int pos)
{
    if (pos < 1 || pos > E.count || E.head == NULL) {
        puts("ERROR:  Invalid position");
        return;
    } 
    if (pos == 1) pos = POS_HEAD;
    if (pos == E.count) pos = POS_TAIL;
    switch (pos)
    {
        case POS_HEAD:
            if(E.head->next == NULL)
            {
              FEMTO_free(E.head);
            } else {
                E.head = E.head->next;
                FEMTO_free(E.head->prev);
                E.head->prev = NULL;
            }
            break;
        case POS_TAIL:
            E.tail = E.tail->prev;
            FEMTO_free(E.tail->next);
            E.tail->next = NULL;
            break;
        default:
        {
            erow_t* temp = E.head;
            erow_t* temp_prev = NULL;
            while (pos-- > 1) temp = temp->next;
            temp_prev = temp->prev;
            temp_prev->next = temp->next;
            temp->next->prev = temp_prev;
            FEMTO_free(temp); 
        }
    }
    E.count--;
}
void Femto_Delete_Lines(uint16_t s, uint16_t e)
{
  for (size_t i = e; i > s; i--) Femto_Delete_Line(i);
}

erow_t* Femto_Insert_Line(int pos, long p, long s)
{
    if (pos < 1 || pos > E.count + 1) {
        puts("ERROR:  Invalid position");
        return NULL;
    } 
    if (pos == 1) pos = POS_HEAD;
    if (pos == E.count + 1) pos = POS_TAIL;
    erow_t* ptr = (erow_t*)FEMTO_malloc(sizeof(erow_t));
    if (ptr == NULL) return NULL; //die("OUT OF MEMORY",__FUNCTION__);
    ptr->next = NULL;
    ptr->prev = NULL;

    if (E.head == NULL) {
        E.head = ptr;
        E.tail = E.head;
    }
    else switch (pos)
    {
        case POS_HEAD:
            ptr->next = E.head;
            E.head->prev = ptr;
            E.head = ptr;
            break;
        case POS_TAIL:
            ptr->prev = E.tail;
            E.tail->next = ptr;
            E.tail = ptr;
            break;
        default:
        {
            erow_t* src = E.head;
            while (pos-->1) src = src->next;
            erow_t **da, **ba;
            ba = &src;
            da = &(src->prev);
            ptr->next = (*ba);
            ptr->prev = (*da);
            (*da)->next = ptr;
            (*ba)->prev = ptr;
        }
    }
    ptr->pos = p;
    ptr->len = s;
    ptr->flags = 2;
    E.count++;
    E.dirty = true;
    return ptr;
}
#ifdef FEMTO_WRITE_BUFFER_SIZE
bool Femto_flush_write_buffer(FILE *__restrict__ __s)
{
  uint16_t buffer_lvl = strlen(E.write_buffer);
  size_t wr = fwrite(E.write_buffer,1,buffer_lvl, __s);
  fflush(__s);
  memset(E.write_buffer, 0, FEMTO_WRITE_BUFFER_SIZE);
  if (wr != buffer_lvl) return false;
  return true;
}
size_t Femto_buffered_write(const void *__restrict__ __ptr, size_t __size, size_t __n, FILE *__restrict__ __s)
{
  uint16_t pos = strlen(E.write_buffer);
  uint16_t len = __size * __n;
  if ((len + pos) > FEMTO_WRITE_BUFFER_SIZE) // incoming data will exceeds buffer size.
  {
    const uint16_t data_size = len - ((len + pos) - FEMTO_WRITE_BUFFER_SIZE);
    strncpy(E.write_buffer + pos,__ptr, data_size);
    // flush buffer
    if (!Femto_flush_write_buffer(__s)) error_message("There was an error saving.");
    // reset buffer
    memset(E.write_buffer,0,FEMTO_WRITE_BUFFER_SIZE);
    strncpy(E.write_buffer,__ptr + data_size, len - data_size);
  } else {
    strncpy(E.write_buffer + pos,__ptr, len);
  }
  return len;
}
#else
static inline bool Femto_flush_write_buffer(void *unused) { return true; }
#define Femto_buffered_write(__ptr, __size, __n, __s) fwrite(__ptr, __size, __n, __s)
#endif 
void Femto_Save()
{
    if (E.origin_fn == NULL)
    {
      prompt_input("filename?");
      if (!buf[0]) return;
      E.origin_fn = FEMTO_malloc(FEMTO_MAX_FNL);
      if (E.origin_fn == NULL)  die("FEMTO_malloc origin_fn", __FUNCTION__);
      strncpy(E.origin_fn,buf,256);
    }
    int i = 0;
    erow_t* line = E.head;
    FILE* tempF = fopen("FEMTO_T","w");
    while (line != NULL)
    {
        long p = line->pos;
        int l = line->len;
        {
          memset(rbuf, 0 , 1021);
          int e = 0;
          switch (line->flags & 3)
          {
            case 0: if (E.origin == NULL) e = -1; else e = fseek(E.origin,p,SEEK_SET); break;
            case 1: if (E.working == NULL) e = -1; else e = fseek(E.working,p,SEEK_SET); break;
            case 2: break; // cached
            default:
              puts("FLAG ERROR");
              return;
          }
          if (e == -1)
          {
            error_message("There was an error saving.");
            return;
          }
          
          switch (line->flags & 3)
          {
            case 0: e = fread(rbuf, 1, l, E.origin); break;
            case 1: e = fread(rbuf, 1, l, E.working); break;
            case 2: e = l; strncpy(rbuf, &E.cache[p], l); break;
          }
          if (e != l) break;
          rbuf[l]='\n';
          line->pos = i;
          i += Femto_buffered_write(rbuf,1,l+1,tempF);
          line->flags = 0; // saved
        }
        line = line->next;
    }
    if (!Femto_flush_write_buffer(tempF)) error_message("There was an error saving.");
    fclose(tempF);
    rename("FEMTO_T",E.origin_fn);
    if (E.origin) fclose(E.origin);
    if (E.working) { fclose(E.working); unlink(E.working_fn); FEMTO_free(E.working_fn); E.working = NULL; }
    E.origin = fopen(E.origin_fn,"r");
    E.dirty = false;
    E.cache_len = 0;
    printf("%d\n\r", i);
}

void Femto_Open(char *filename) {
    E.origin_fn = FEMTO_malloc(FEMTO_MAX_FNL);
    if (E.origin_fn == NULL)  die("FEMTO_malloc origin_fn", __FUNCTION__);
    strcpy(E.origin_fn,filename);
    E.origin = fopen(E.origin_fn, "r");
    if (!E.origin) {
      error_message(strerror(errno));
      return;
    }
    char linebuff[256] = { 0 };
    char *cp, *bufPtr, *bufBase = linebuff;
        int cc = 0, charCount = 0, lineCount = 0 ;
    ssize_t linelen = 0;
    ssize_t file_pos = 0;
    bufPtr = bufBase;
    int bufUsed = 0;
    erow_t *temp = NULL;
    do {
        cp = memchr(bufPtr, '\n', bufUsed);

        if (cp) {
            linelen = (cp - bufPtr) + 1;
            
            bufPtr += linelen;
            bufUsed -= linelen;
            temp = Femto_Insert_Line(E.count + 1, file_pos, linelen - 1);
            if (temp) temp->flags = 0; else {
              error_message("Unable to open file, Out of memory.");
              return;
            }
            file_pos += linelen;
            charCount += linelen;
            lineCount++;
            /* num++; */
            continue;
        }

        if (bufPtr != bufBase) {
            memcpy(bufBase, bufPtr, bufUsed);
            bufPtr = bufBase + bufUsed;
        }

        cc = fread(bufPtr, 1, 256 - bufUsed,E.origin);
        bufUsed += cc;
        bufPtr = bufBase;
    } while (cc > 0);
    if (bufUsed) {
        temp = Femto_Insert_Line(E.count + 1, file_pos, linelen);
        if (temp) temp->flags = 0;
        lineCount++;
        charCount += bufUsed;
    }
    printf("%d\r\n",charCount);
    E.dirty = false;
}
int Femto_Cache_flush()
{
    if (E.working_fn == NULL)  
    {
      E.working_fn = FEMTO_malloc(FEMTO_MAX_FNL);
      if (E.working_fn == NULL)  die("FEMTO_malloc working_fn", __FUNCTION__);
      strncpy(E.working_fn,"FEMTO_TW",256);
    } /* else fprintf(stderr,"Cache is set\n\r"); */
    int i = 0;
    erow_t* line = E.head;
    if (E.working == NULL) E.working = fopen(E.working_fn,"a+");/*  else fprintf(stderr,"E.cache is open\n\r"); */
    if (E.working == NULL) die("FAILED TO OPEN E.working", __FUNCTION__);
    int e = fseek(E.working,0L,SEEK_END);
    if (e == -1)
    {
      error_message("There was an error seeking to EOF.");
      return -1;
    }
    while (line != NULL)
    {
        /* fprintf(stderr,"%p : %d %d  %d\n\r", line, line->pos, line->len, line->flags); */
        int l = line->len;
        memset(rbuf, 0 , 1021);
        if (line->flags != 2) { line = line->next; continue; } 
        if (e == -1)
        {
          error_message("There was an error saving.");
          return -1;
        }
        long p = ftell(E.working);
        strncpy(rbuf, &E.cache[line->pos], l);
        rbuf[l]='\n';
        line->pos = p;
        i += fwrite(rbuf,1,l+1,E.working);
        line->flags = 1; // cached to disk
        line = line->next;
    }
    E.cache_len = 0;
    return 1;
}
int Femto_Cache_text(const char* d)
{
  if (E.cache_len >= FEMTO_CACHE_SIZE - strlen(d)) Femto_Cache_flush();//die("CACHE FULL!", __FUNCTION__);
  uint16_t p = E.cache_len;
  for (const char* c = d; *c != 0;c++)
  {
    E.cache[E.cache_len++] = *c;
  }
  return p;
}
void Femto_Load_line_to_buffer(uint16_t line)
{
    memset(buf,0, FEMTO_MAX_FNL);
    int i = 1, e = 0;
    char c = '\0';
    erow_t* ptr = E.head;
    while (ptr != NULL)
    {
        if (i == line)
        { 
            switch (ptr->flags & 3)
            {
              case 0: if (E.origin == NULL) e = -1; else e = fseek(E.origin,ptr->pos,SEEK_SET); break;
              case 1: if (E.working == NULL) e = -1; else e = fseek(E.working,ptr->pos,SEEK_SET); break;
              case 2: break; // cached
              default:
                puts("FLAG ERROR");
                return;
            }
            for (size_t i = 0; i < ptr->len; i++)
            {
              switch (ptr->flags & 3)
              {
                case 0: e = fread(&c, 1, 1, E.origin); break;
                case 1: e = fread(&c, 1, 1, E.working); break;
                case 2: e = 1; c = E.cache[ptr->pos + i]; break;
              }
              if (e != 1) break;
              if (c == '\n') return;
              buf[i] = c;
            }
            return;
        } else { i++; }
        ptr = ptr->next;
    }
}
void Femto_Render_Row(erow_t *line, bool full_line)
{
  int idx = 7;
  char c = '\0';
  int e = 0;
  switch (line->flags & 3)
  {
    case 0: if (E.origin == NULL) e = -1; else e = fseek(E.origin,line->pos,SEEK_SET); break;
    case 1: if (E.working == NULL) e = -1; else e = fseek(E.working,line->pos,SEEK_SET); break;
    case 2: break; // cached
    default:
      puts("FLAG ERROR");
      return;
  }
  if (e == -1)
  {
    printf("<%d,%d : %s>\n\n",line->pos,line->len, strerror(errno));
    return;
  }
  for (size_t i = 0; i < line->len; i++)
  {
    idx++;
    switch (line->flags & 3)
    {
      case 0: e = fread(&c, 1, 1, E.origin); break;
      case 1: e = fread(&c, 1, 1, E.working); break;
      case 2: e = 1; c = E.cache[line->pos + i]; break;
    }
    if (e != 1) break;
    if (c == '\t') {
      putchar(' ');
      while (idx % FEMTO_TABSTOP != 0) { idx++;  putchar(' '); }
    } else {
      putchar(c);
    }
    if (c == '\n') {
      break;
    }
    if (idx > E.screen_cols -1 && !full_line) { printf("\x1b[7m>\x1b[m"); return; }
  }
  putchar('\r');
  if (c != '\n') putchar('\n');
}

void Femto_print_range(uint16_t s, uint16_t e, bool full_line)
{
    int i = 1;
    if (e <= 0) e = UINT16_MAX;
    erow_t* ptr = E.head;
    while (ptr != NULL)
    {
        if (i >= s && i <= e)
        {
            printf("\x1b[7;1m");
            if (ptr->flags)
            {
                putchar('*');
            } else {
                putchar(' ');
            }
            printf ("%04d\x1b[m ",i++);
            Femto_Render_Row(ptr, full_line);
        } else { i++; }
        ptr = ptr->next;
    }
}


void Femto_Insert()
{
  do {
    printf("\r \x1b[1m:\x1b[m");
    fflush(stdout);
    Line_Editor_Process_Keypress(FEMTO_TABSTOP,false);
    if (buf[0] == '.' && buf[1] == 0) break;
    Femto_Insert_Line(E.current_line++,Femto_Cache_text(buf),strlen(buf)); 
  } while (true);
}

int Femto_command()
{
    int num[2] = {-1,-1}, num_index = 0;
    char p = '\0', cmd = '\0', *str = NULL;
    bool force = false;
    printf("\r\x1b[32;1mCMD:\x1b[m");
    fflush(stdout);
    Line_Editor_Process_Keypress(5, false);
    for (char *c = buf;*c != 0;c++)
    {
        if (isblank(*c)) continue;
        if (isalpha(*c))
        {
            if (cmd) { error_message("Too many commands"); return EXIT_FAILURE; }
            *c = tolower(*c);
            cmd = *c;
        } 
        if (isdigit(*c))
        {
            int r;
            sscanf(c,"%d%n",&num[num_index], &r);
            c+= (r-1);
            continue;
        }
        if (ispunct(*c))
        {
            if (p) { error_message("Too many modifiers"); return EXIT_FAILURE; }
            switch (*c)
            {
              case '=': //current Line
                num[num_index] = E.current_line;
                break;
              case '!': force = true; break;
              case '+': // advance line
              case '-': //previous line
                if (cmd) { error_message("Too many commands"); return EXIT_FAILURE; }
                cmd = *c;
                break;
              case ':':
                str = c + 1;
                num_index = 2; // Force exit
                break;
              case '.': //Last line
                num[num_index] = E.count;
                break;
              case ',':
                num_index++;
                p = *c;
                break;
              case '#': //toggle line numbers 
              default:
                error_message("Modifier not allowed"); return EXIT_FAILURE;
            }
            if (num_index >= 2) break; 
            continue;
        }
    }
    switch (cmd)
    {
        case 'a': //append
            if (num[0] == -1) num[0] = E.current_line;
            num[0]++;
            if (num[0] > E.count + 1 && !(E.count==0 && num[0] == 2)) 
            {
              error_message("line out of range");
              break;
            }
            if (E.count==0) num[0] = 1;
            E.current_line = num[0];
            if (str == NULL) Femto_Insert();
            else Femto_Insert_Line(E.current_line++,Femto_Cache_text(str),strlen(str)); 
            break;
        case 'b': // bug
            printf("Current Line %d\n\rLine Count %d\n\rE.cache Used %d\n\rDirty %c\n\r",
                E.current_line,
                E.count,
                E.cache_len,
                E.dirty ? 'Y' : 'n'
                );
            break;
        case 'd': //delete
            if (num[0] == -1 && p == 0) num[0] = E.current_line;
            else if (num[0] == -1 && p == ',') num[0] = 1;
            if (num[1] == -1 && p) num[1] = E.count;
            if (num[0] > num[1]) num[1] = num[0];
            if (num[1] > E.count)
            {
              error_message("line out of range");
              break;
            }
            Femto_Delete_Lines(num[0],num[1]);
            E.current_line = num[1];
            Femto_Delete_Line(num[0]);
            E.current_line = num[0];
            break;
        case 'c': //change
            if (num[0] == -1) num[0] = E.current_line;
            if (num[0] > E.count)
            {
              error_message("line out of range");
              break;
            }
            Femto_Delete_Line(num[0]);
            E.current_line = num[0];
            Femto_Insert();
            break;
        case 'e': //edit line
            if (num[0] == -1) num[0] = E.current_line;
            if (num[0] > E.count)
            {
              error_message("line out of range");
              break;
            }
            E.current_line = num[0];
            printf("\r \x1b[1m:\x1b[m");
            fflush(stdout);
            Femto_Load_line_to_buffer(E.current_line);
            Line_Editor_Process_Keypress(FEMTO_TABSTOP,true);
            Femto_Insert_Line(E.current_line++,Femto_Cache_text(buf),strlen(buf)); 
            Femto_Delete_Line(E.current_line);
            break;
        case 'f': //flags
            error_message("command not implemented");
            break;
        case 'i': //insert
            if (num[0] == -1) num[0] = E.current_line;
            if (num[0] > E.count && !(num[0] == 1 && E.count==0))
            {
              error_message("line out of range");
              break;
            }
            E.current_line = num[0];
            if (str == NULL)
              Femto_Insert();
            else
              Femto_Insert_Line(E.current_line++,Femto_Cache_text(str),strlen(str));
            break;
        case 'm': //memory usage
#if USE_PTMEM == 1
            printf("MEMORY USED : %u FREE: %u\n", pt_get_used_memory(), pt_get_free_memory());
#else
            printf("MEMORY USAGE NOT AVAILABLE\n");
#endif
            break;
        case 'l': //full line
            if (num[0] == -1 && p == 0) num[0] = E.current_line;
            else if (num[0] == -1 && p == ',') num[0] = 1;
            if (num[1] == -1 && p) num[1] = E.count;
            if (num[0] > num[1]) num[1] = num[0];
            if (num[1] > E.count)
            {
              error_message("line out of range");
              break;
            }
            Femto_print_range(num[0],num[1],true);
            E.current_line = num[1];
            break;
        case 'p': //print'
            if (num[0] == -1 && p == 0) num[0] = E.current_line;
            else if (num[0] == -1 && p == ',') num[0] = 1;
            if (num[1] == -1 && p) num[1] = E.count;
            if (num[0] > num[1]) num[1] = num[0];
            if (num[1] > E.count)
            {
              error_message("line out of range");
              break;
            }
            Femto_print_range(num[0],num[1], false);
            E.current_line = num[1];
            break;
        case 'q': //quit :
            if (E.dirty && !force) {
                prompt_input("Really Quit?");
                if (tolower(buf[0]) != 'y') break; 
            }
            E.running = false;
            break;
        case 'r': //read : insert line from another file
        case 's': //search?
            error_message("command not implemented");
            break;
        case 'w': //write :
            if (str) {
              if (E.origin_fn == NULL)
              {
                E.origin_fn = FEMTO_malloc(FEMTO_MAX_FNL);
                if (E.origin_fn == NULL)  die("FEMTO_malloc origin_fn", __FUNCTION__);
              }
              strncpy(E.origin_fn,buf,256);
            }
            Femto_Save();
            break;
        case 'v': // version
            puts(FEMTO_VERSION);
            break;
        case '\0': //next line
          if (num[0] == 0 || num[0] > E.count || E.current_line > E.count)
          {
              error_message("line out of range");
              break;
          }
          if (num[0] > 0)
          {
              E.current_line = num[0];
              Femto_print_range(num[0],num[0],false);
              break;
          }
          E.current_line++;
          if (E.current_line > E.count) { error_message("line out of range"); break; }
          Femto_print_range(E.current_line,E.current_line,false);
          break;
        case '+': // advance line
          if (num[0] == -1) num[0] = 1;
          if ((E.current_line + num[0]) <= E.count) 
          {
            E.current_line += num[0];
            Femto_print_range(E.current_line,E.current_line,false);
            break;
          }
          error_message("line out of range");
          break;
        case '-': //previous line
          if (num[0] == -1) num[0] = 1;
          if ((E.current_line - num[0]) >= 1) 
          {
            E.current_line -= num[0];
            Femto_print_range(E.current_line,E.current_line,false);
            break;
          }
          error_message("line out of range");
          break;
        default:
            error_message("command not implemented");
            break;
    }
    if (E.current_line > E.count && E.count !=0) E.current_line = E.count;
    return EXIT_SUCCESS;
}

void Femto_Initialize()
{
#if FEMTO_PICO_LIB == 0
    setlocale(LC_CTYPE, "UTF-8");
	  enableRawMode();
#endif
    E.screen_cols = FEMTO_MAX_COL;
    E.count = 0;
    E.tail = NULL;
    E.head = NULL;
    E.running = true;
    E.current_line = 0;
    E.working_fn = NULL;
    E.origin_fn = NULL;
    E.current_line = 1;
    E.cache = FEMTO_malloc(FEMTO_CACHE_SIZE);
#ifdef FEMTO_WRITE_BUFFER_SIZE
    E.write_buffer = FEMTO_calloc(FEMTO_WRITE_BUFFER_SIZE + 1,1);
#endif
    E.cache_len = 0;
}

#ifdef FEMTO_PICO_LIB
int app_femto(int argc, char** argv)
{
  Femto_Initialize();

    if (argc >= 2) {
        Femto_Open(argv[1]);
    } else {
        puts("No file\r");
    }
    while(E.running)
    {
        Femto_command();
    }
    if (E.origin) fclose(E.origin);
    if (E.origin_fn) FEMTO_free(E.origin_fn);
    if (E.working) { fclose(E.working); unlink(E.working_fn);}
    FEMTO_free(E.cache);
    return 0;
}
#else
int main(int argc, char** argv)
{
    Femto_Initialize();

    if (argc >= 2) {
        Femto_Open(argv[1]);
    } else {
        puts("No file\r");
    }
    while(E.running)
    {
        Femto_command();
    }
    if (E.origin) fclose(E.origin);
    if (E.origin_fn) FEMTO_free(E.origin_fn);
    if (E.working) { fclose(E.working); unlink(E.working_fn);}
    Femto_Delete_Lines(1,E.count);
#ifdef FEMTO_WRITE_BUFFER_SIZE
    FEMTO_free(E.write_buffer);
#endif
    return 0;
}
#endif