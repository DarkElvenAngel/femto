#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>

#ifdef USE_PT_IO
#include "pt52io.h"
#endif

#define USERSIZE 255
#define CTRL_KEY(k) ((k) & 0x1f)
#define TAB_STOP 4

enum editorKey {
  BACKSPACE = 127,
  ARROW_LEFT    = 1000,
  ARROW_RIGHT,
  ARROW_UP,
  ARROW_DOWN,
  INS_KEY,
  DEL_KEY,
  HOME_KEY,
  END_KEY,
  PAGE_UP,
  PAGE_DOWN,
};

uint8_t INS_MODE = 0;
char buf[USERSIZE + 1];
int pos = 0;
int rpos = 0;
char rbuf[(USERSIZE * TAB_STOP) + 1];

/// @brief Line Editor Start
int Line_Editor_Cursor_Postion(int cx) {
  int rx = 0;
  for (int j = 0; j < cx; j++) {
    if (buf[j] == '\t' && cx == j + 1)
      return rx + 1;
    if (buf[j] == '\t')
      rx += (TAB_STOP - 1) - (rx % TAB_STOP);
    rx++;
  }
  return rx;
}
void Line_Editor_Render(uint8_t margin) {
  if (margin) printf("\r\x1b[%dC\x1b[K",margin);
  else printf("\r\x1b[2K");
  int idx = 0;
  int offset = 0;
  int rp = Line_Editor_Cursor_Postion(pos + 1);
  if (rp < offset) {
    offset = rp;
  }
  if (rp >= offset + (80 - margin)) {
    offset = rp - (80 - margin) + 1;
  }
  memset(rbuf,0,1021);
  char *rb = rbuf;
  for (size_t i = 0; i < strlen(buf); i++)
  {
    char c = buf[i];
    idx++;
    if (c == '\t') {
      *rb = ' ';
      while ((idx) % TAB_STOP != 0) { idx++;  *++rb = ' '; }
    } else { *rb = c; }
    if (c == '\n') { break; }
    ++rb;
  }
  rb = rbuf + offset;
  for (idx = 0;*rb;rb++){ putchar(*rb); if (++idx > (78 - margin)) { break; }}
  if (rp - offset - 1 || margin)
    printf("\r\x1b[%dC",(rp - offset - 1) + margin);
  else
    printf("\r");
	fflush(stdout);
}
void Line_Editor_RowDel_Char(int at) {
  if (at < 0 || at >= strlen(buf)) return;
  memmove(&buf[at], &buf[at + 1], strlen(buf) - at);
}
void Line_Editor_RowInsert_Char( int at, int c) {
  if (at < 0 || at > strlen(buf)) at = strlen(buf);
  memmove(&buf[at + 1], &buf[at], strlen(buf) - at + 1);
  buf[at] = c;
}
void Line_Editor_Del_Char() {
  if (pos == 0 && strlen(buf) == 0) return;
  if (pos > 0) {
    Line_Editor_RowDel_Char(pos - 1);
    pos--;
  }
}
uint16_t Line_Editor_ReadKey() {
#ifdef USE_PT_IO
  keycode_t k = get_lastkey();
  switch (k.keycode)
  {
    case 0x49 : return INS_KEY;
    case 0x4a : return HOME_KEY;
    case 0x4c : return DEL_KEY;
    case 0x4d : return END_KEY;
    case 0x4b: return PAGE_UP;
    case 0x4e : return PAGE_DOWN;
    // case 0x5f : return HOME_KEY;
    // case 0x59 : return END_KEY;
    case 0x52 : return ARROW_UP;
    case 0x51 : return ARROW_DOWN;
    case 0x4f : return ARROW_RIGHT;
    case 0x50 : return ARROW_LEFT;
    default : return k.character;
  }
#else
  int nread;
  char c;
  while ((nread = read(STDIN_FILENO, &c, 1)) != 1) {
    if (nread == -1 && errno != EAGAIN) return 0;
  }
  if (c == '\x1b') {
    char seq[3];
    if (read(STDIN_FILENO, &seq[0], 1) != 1) return '\x1b';
    if (read(STDIN_FILENO, &seq[1], 1) != 1) return '\x1b';
    if (seq[0] == '[') {
      if (seq[1] >= '0' && seq[1] <= '9') {
        if (read(STDIN_FILENO, &seq[2], 1) != 1) return '\x1b';
        if (seq[2] == '~') {
          switch (seq[1]) {
            case '1': return HOME_KEY;
            case '2': return INS_KEY;
            case '3': return DEL_KEY;
            case '4': return END_KEY;
            case '5': return PAGE_UP;
            case '6': return PAGE_DOWN;
            case '7': return HOME_KEY;
            case '8': return END_KEY;
          }
        }
      } else {
        switch (seq[1]) {
          case 'A': return ARROW_UP;
          case 'B': return ARROW_DOWN;
          case 'C': return ARROW_RIGHT;
          case 'D': return ARROW_LEFT;
          case 'H': return HOME_KEY;
          case 'F': return END_KEY;
        }
      }
    } else if (seq[0] == 'O') {
      switch (seq[1]) {
        case 'H': return HOME_KEY;
        case 'F': return END_KEY;
      }
    }
    return '\x1b';
  } else {
    return c;
  }
#endif
}
void Line_Editor_Move_Cursor(uint16_t key) {
  switch (key) {
    case ARROW_LEFT:
      if (pos != 0) { pos--; }
      break;
    case ARROW_RIGHT:
      if (pos < strlen(buf))
      { pos++; }
      break;
  }
}
void Line_Editor_Process_Keypress(uint8_t margin, bool skip_clean) { 
  if (!skip_clean) memset(buf,0,sizeof(buf));
  pos = 0;
  Line_Editor_Render(margin);
  while (1) {
    uint16_t c = Line_Editor_ReadKey();
    switch (c) {
      case '\r':
        pos = strlen(buf);
        Line_Editor_Render(margin);
        puts("\r");
        return;
      case HOME_KEY:
        pos = 0;
        break;
      case END_KEY:
          pos = strlen(buf);
        break;
      case INS_KEY:
        INS_MODE = INS_MODE == 0 ? 1 : 0;
        break;
      case BACKSPACE:
      case CTRL_KEY('h'):
      case DEL_KEY:
        if (c == DEL_KEY) Line_Editor_Move_Cursor(ARROW_RIGHT);
        Line_Editor_Del_Char();
        break;
      case ARROW_LEFT:
      case ARROW_RIGHT:
        Line_Editor_Move_Cursor(c);
        break;
      default: 
if(c < 0x20) break;   // Unprocessed control character
        if(INS_MODE) 
          buf[pos++] = c;
        else 
          Line_Editor_RowInsert_Char(pos++, c);
    }
    Line_Editor_Render(margin);
  }
}
/// END LINE EDITOR
char* Line_Editor_ReadLine(const char* prompt) { 
  if (prompt != NULL) printf(prompt);
  memset(buf,0,sizeof(buf));
  pos = 0;
  const uint8_t margin = strlen(prompt);
  Line_Editor_Render(margin);
  while (1) {
    uint16_t c = Line_Editor_ReadKey();
    switch (c) {
      case '\r':
        pos = strlen(buf);
        Line_Editor_Render(margin);
        puts("\r");
        return buf;
      case HOME_KEY:
        pos = 0;
        break;
      case END_KEY:
          pos = strlen(buf);
        break;
      case INS_KEY:
        INS_MODE = INS_MODE == 0 ? 1 : 0;
        break;
      case BACKSPACE:
      case CTRL_KEY('d'): return NULL;
      case CTRL_KEY('h'):
      case DEL_KEY:
        if (c == DEL_KEY) Line_Editor_Move_Cursor(ARROW_RIGHT);
        Line_Editor_Del_Char();
        break;
      case ARROW_LEFT:
      case ARROW_RIGHT:
        Line_Editor_Move_Cursor(c);
        break;
      default: 
        if(c < 0x20) break;   // Unprocessed control character
        if(INS_MODE) 
          buf[pos++] = c;
        else 
          Line_Editor_RowInsert_Char(pos++, c);
    }
    Line_Editor_Render(margin);
  }
}
