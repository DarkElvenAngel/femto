# Femto - The smaller text editor

## Purpose

Femto is designed for low memory systems and is based on **ed** the default editor for linux.  
Femto has a small code base and doesn't pack in a ton of features the goal is to keep things small. It's also designed to work with **ptsh** for the RP2040 and RP235X see details below.

## Complied size Comparison

For the Linux build femto has a small binary size

``` text
-rwxr-xr-x 1 root root 23K Jul  1 2023 /usr/bin/femto
-rwxr-xr-x 1 root root 51K Jan 15  2021 /usr/bin/ed
-rwxr-xr-x 1 root root 1.4M Sep 30  2021 /usr/bin/vim.tiny
```

## Usage

Femto takes one optional argument this is the filename you what to edit. There are no command line options. Femto doesn't accept piped input.  Once the interface is open you will be at the CMD: command prompt using single letter commands and modifiers you are able to edit files.  

### Commands

* a - append lines at current line
* b - print status
* c - change contents of current line (delete and insert a newline)
* d - delete current line
* e - edit current line
* i - insert line at current line
* l - long print lines (Prints full lines)
* n - line numbers on/off
* p - print lines (Prints lines truncated to screen width)
* q - quit
* r - read file to current line
* w - write file to disk
* v - print version information
* \+ - move forward *n* lines
* \- - move backward *n* lines

### Short cuts keys

* . - last line
* = - current line
* : - start string

### Range selection

* *n*,*n*

## How commands work

Commands are entered at the **CMD:** prompt the format isn't restrictive like in **ed**  
for example if you want to print line 12 in femto `12p` or `p12` the order doesn't much matter.  
If you just need to insert one line you can do it right for the command prompt with the start string shortcut **:**

``` text
CMD: i:Hello World
CMD: p
*0001 Hello World
CMD:
```

Also all white spaces are ignored in Femto commands up to the start string shortcut **:**

## How to include Femto in your own project

Femto is designed to be used as submodule simply add it to your project and then add to your CMakeLists.txt

```cmake
add_subdirectory(femto)

target_link_libraries(
  ${BINARY}

  PRIVATE
  femto
  )
```

During you `cmake ..`  step you will see a message `[cmake] Femto - Build As PICO Library` This will indicate everything is working correctly.

There are some optional values you can set to define how Femto is configured

* **FEMTO_MAX_COL** How wide is the terminal. Default is 80
* **FEMTO_CACHE_SIZE** Femto uses a working cache to store changes before flushing them to disk. Default is 1024
* **FEMTO_WRITE_BUFFER_SIZE** Femto has an optional write buffer that is used during save operations this is useful to make less writes to the file system. Default is not set (OFF)
* **USE_PT_IO** Femto is optimized for use with PT52 if used with a compatible project set this flag. Default is unset.

Femto uses Heap memory allocation you can define alternative functions to use for this.

* **FEMTO_malloc** by default uses `malloc`.
* **FEMTO_calloc** by default uses `calloc`.
* **FEMTO_realloc** by default uses `realloc`.
* **FEMTO_free** by default uses `free`.
